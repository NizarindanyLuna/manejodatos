package modelos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vistas.dlgManejoDatos;


public class DBProducto extends DbManejador implements DbPersistencia {
    dlgManejoDatos vista;
    
    @Override
    public void insertar(Object objecto) throws Exception{

        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "Insert into "
                + "productos(codigo,nombre,fecha,precio,STATUS)values(?,?,?,?,?)";

        if (this.conectar()) {
            try {
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);

                //asignar los valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al insertar " + e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista,"No fue posible conectarse ");
        }

        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        
        Productos pro=new Productos();
        pro=(Productos) objecto;
        
        String consulta="update productos set nombre = ?, precio = ?" + "fecha = ? where codigo = ?";
        
        if(this.conectar()){
            try{
                System.out.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar los valores a los campos
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                JOptionPane.showMessageDialog(vista,"Actualizado correctamente...");
                
            }catch(SQLException e){
                JOptionPane.showMessageDialog(vista,"ERROR al insertar..." + e.getMessage());
            }
        }
    }

    @Override
    public void habilitar(Object objecto) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    
        Productos pro=new Productos();
        pro = (Productos) objecto;
        
        String consulta ="update productos set status = 0 where codigo = ?";
        if(this.conectar()){
            try{
                JOptionPane.showMessageDialog(vista,"status habilitado correctamente...");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                JOptionPane.showMessageDialog(vista,"ERROR al actualizar status..."+ e.getMessage());
            }
        }
    
    }

    @Override
    public void deshabilitar(Object objecto) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        Productos pro = new Productos();
        pro = (Productos) objecto;
        
        String consulta = "UPDATE productos SET status = 1 WHERE codigo = ?";
        if(this.conectar()){
            try{
                JOptionPane.showMessageDialog(vista,"Se deshabilito correctamente");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, pro.getCodigo());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                
            }catch(SQLException e){
                JOptionPane.showMessageDialog(vista,"ERROR al actualizar status..." +e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(int id) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

        return true;

    }

    @Override
    public DefaultTableModel listar() throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

        Productos pro;

        if (this.conectar()) {
            String consulta = "Select * from productos where status = 0 order by codigo";
            DefaultTableModel tabla=new DefaultTableModel();
            
            PreparedStatement pst= conexion.prepareStatement(consulta);
            ResultSet rs= pst.executeQuery();
            ResultSetMetaData datos= rs.getMetaData();
            
            for(int column=1; column < datos.getColumnCount(); column++){
                tabla.addColumn(datos.getColumnLabel(column));
            }
            while(rs.next()){
                Object[] row=new Object[datos.getColumnCount()];
                for(int column=1; column <= datos.getColumnCount(); column++){
                    row[column-1] = rs.getString(column);
                }
                tabla.addRow(row);
            }
            return tabla;
        }
        return null;
    }

    @Override
    public DefaultTableModel listaD() throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        
        Productos pro;

        if (this.conectar()) {
            String consulta = "Select * from productos where status = 1 order by codigo";
            DefaultTableModel tabla=new DefaultTableModel();
            
            PreparedStatement pst= conexion.prepareStatement(consulta);
            ResultSet rs= pst.executeQuery();
            ResultSetMetaData datos= rs.getMetaData();
            
            for(int column=1; column < datos.getColumnCount(); column++){
                tabla.addColumn(datos.getColumnLabel(column));
            }
            while(rs.next()){
                Object[] row=new Object[datos.getColumnCount()];
                for(int column=1; column <= datos.getColumnCount(); column++){
                    row[column-1] = rs.getString(column);
                }
                tabla.addRow(row);
            }
            return tabla;
        }
        return null;
         
    }

    @Override
    public Object buscarD(String codigo) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

        Productos pro = new Productos();

        if (this.conectar()) {
            String consulta = "Select * from productos where codigo= ? and status=1";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //asignar valores
            this.sqlConsulta.setString(1, codigo);
            //hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros

            if (this.registros.next()) {
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setStatus(this.registros.getInt("Status"));
                
            }

        }
        this.desconectar();
        return pro;

    }

    @Override
    public Object buscar(String codigo) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet.");

        Productos pro = new Productos();
        

        if (this.conectar()) {
            String consulta = "Select * from productos where codigo= ? and status=0";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //asignar valores
            this.sqlConsulta.setString(1, codigo);
            //hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros

            if (this.registros.next()) {
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setStatus(this.registros.getInt("Status"));
            }

        }
        this.desconectar();
        return pro;
    }
}
